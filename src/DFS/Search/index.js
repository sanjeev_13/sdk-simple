import React, { memo, useCallback, useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Input, Paper } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    height: theme.spacing(6),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: theme.spacing(2),
    backgroundColor: theme.palette.background.default,
  },
  iconButton: {
    color: theme.palette.action.active,
    transform: 'scale(1, 1)',
    transition: theme.transitions.create(['transform', 'color'], {
      duration: theme.transitions.duration.shorter,
      easing: theme.transitions.easing.easeInOut,
    }),
  },
  iconButtonHidden: {
    transform: 'scale(0, 0)',
    '& > $icon': {
      opacity: 0,
    },
  },
  searchIconButton: {
    marginRight: theme.spacing(-6),
  },
  input: {
    width: '100%',
  },
  searchContainer: {
    margin: 'auto 16px',
    width: `calc(100% - ${theme.spacing(6 + 4)}px)`,
  },
}));

const SearchInput = React.forwardRef(
  (
    {
      cancelOnEscape,
      className,
      disabled,
      onCancelSearch,
      onRequestSearch,
      style,
      value,
      onFocus,
      onBlur,
      onChange,
      onKeyUp,
      ...inputProps
    },
    ref
  ) => {
    const classes = useStyles();
    const [inputValue, setInputValue] = useState(value);

    useEffect(() => {
      setInputValue(value);
    }, [value]);

    const handleFocus = useCallback(
      (e) => {
        if (onFocus) {
          onFocus(e);
        }
      },
      [onFocus]
    );

    const handleBlur = useCallback(
      (e) => {
        setInputValue((v) => v.trim());
        if (onBlur) {
          onBlur(e);
        }
      },
      [onBlur]
    );

    const handleInput = useCallback(
      (e) => {
        setInputValue(e.target.value);
        if (onChange) {
          onChange(e.target.value);
        }
      },
      [onChange]
    );

    const handleCancel = useCallback(() => {
      setInputValue('');
      if (onCancelSearch) {
        onCancelSearch();
      }
    }, [onCancelSearch]);

    const handleRequestSearch = useCallback(() => {
      if (onRequestSearch) {
        onRequestSearch(value);
      }
    }, [onRequestSearch, value]);

    const handleKeyUp = useCallback(
      (e) => {
        if (e.charCode === 13 || e.key === 'Enter') {
          handleRequestSearch();
        } else if (
          cancelOnEscape &&
          (e.charCode === 27 || e.key === 'Escape')
        ) {
          handleCancel();
        }
        if (onKeyUp) {
          onKeyUp(e);
        }
      },
      [handleRequestSearch, cancelOnEscape, handleCancel, onKeyUp]
    );

    return (
      <Paper className={classes.root} style={style} elevation={0}>
        <div className={classes.searchContainer}>
          <Input
            {...inputProps}
            fullWidth
            disableUnderline
            inputRef={ref}
            onBlur={handleBlur}
            value={inputValue}
            onChange={handleInput}
            onKeyUp={handleKeyUp}
            onFocus={handleFocus}
            className={classes.input}
            disabled={disabled}
          />
        </div>
      </Paper>
    );
  }
);

export default memo(SearchInput);
