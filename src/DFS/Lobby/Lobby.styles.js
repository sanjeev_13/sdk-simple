import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {
    padding: theme.spacing(1)
  },
  lobbyHeader: {
    margin: theme.spacing(1, 0),
    [theme.breakpoints.down('sm')]: {
      padding: '0px 15px'
    },
    '& .MuiButton-root': {
      backgroundColor: '#0041c1'
    }
  },
  lobbyContainer: {
    backgroundColor: '#282837',
    padding: theme.spacing(2),
    borderRadius: '10px',
    boxShadow: '0px 4px 12px 0px rgba(0,0,0,0.1)',
    '& .lobbyLeagueList button': {
      dispaly: 'flex',
      alignItems: 'center'
    }
  },
  lobbyleftWrapp: {
    padding: '0px 10px',
    '& .searchContainer ': {
      '& input': {
        color: '#8388A6',
        '&:placeholder': {
          color: '#8388A6'
        }
      }
    },
    '& .makeStyles-root': {
      borderRadius: '8px'
    }
  },
  lobbyLeftPanel: {
    backgroundColor: '#282837',
    padding: '10px',
    borderRadius: '10px',
    marginTop: '20px',

    '& button': {
      borderRadius: '8px',
      padding: '6px',
      backgroundColor: '#363648'
    },
    '& .MuiButton-containedSecondary': {
      backgroundColor: '#1b53c1'
    }
  },

  lobbyMiddleWrapp: {
    '& .middle-lobby-table td button': {
      backgroundColor: '#0041c1',
      borderRadius: '8px'
    }
  }
}));
