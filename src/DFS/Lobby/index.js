import React, { useEffect, useState } from 'react';
import { Grid, Box, Button, Typography } from '@material-ui/core';

import { fetchLobbyMasterData, fetchLobbyContestList } from '../../dfsApi';
import { dfsConfig } from '../../config';

import Search from '../Search';
import MiddleLobby from '../MiddleLobby';

import useStyles from './Lobby.styles';

function Lobby() {
  const [search, setSearch] = useState('');
  const [lobbyLoading, setLobbyLoading] = useState(false);
  const [lobbyMasterLoading, setLobbyMasterLoading] = useState(false);
  const [contests, setContests] = useState([]);
  const [contestMaster, setContestMaster] = useState([]);

  const classes = useStyles();

  const getContestListData = async (params) => {
    setLobbyLoading(true);
    const data = await fetchLobbyContestList(params);
    setContests(data.contest);
    setLobbyLoading(false);
  };

  const getContestMasterData = async () => {
    setLobbyMasterLoading(true);
    const data = await fetchLobbyMasterData();
    setContestMaster(data);
    setLobbyMasterLoading(false);
  };

  useEffect(() => {
    // getContestListData(dfsConfig.contestListParams);
  }, []);

  useEffect(() => {
    // getContestMasterData();
  }, []);

  const handleSearch = (value) => {
    let filterData = [...contests];
    filterData = filterData.filter(
      (item) =>
        item.contest_name.toLowerCase().search(value.toLowerCase()) != -1
    );
    setContests(filterData);
    setSearch(value);
  };

  const handleCancelSearch = () => {
    let filterData = [...contests];
    setContests(filterData);
    setSearch('');
  };

  return (
    <>
      <Grid container>
        <Grid container spacing={1}>
          <Grid item xs={12} lg={2} md={3}>
            <Box className={classes.lobbyleftWrapp}>
              <Grid container>
                <Grid item xs={12}>
                  <Search
                    value={search}
                    placeholder="Search contest name"
                    onChange={(newValue) => handleSearch(newValue)}
                    onCancelSearch={handleCancelSearch}
                  />
                </Grid>
              </Grid>
            </Box>
          </Grid>

          <Grid item xs={12} lg={8} md={6}>
            <Box className={classes.lobbyMiddleWrapp}>
              <Grid
                container
                justifyContent="space-between"
                className={classes.lobbyHeader}
              >
                <Grid item>
                  <Typography variant="button">Lobby</Typography>
                </Grid>
                <Grid item>
                  <Grid container spacing={2}>
                    <Grid item>
                      <Button
                        color="secondary"
                        variant="contained"
                        disableElevation
                      >
                        Create a Contest
                      </Button>
                    </Grid>
                    <Grid item>
                      <Button
                        color="secondary"
                        variant="contained"
                        disableElevation
                      >
                        Join the private league
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>

              <Grid container className={classes.lobbyContainer}>
                {lobbyMasterLoading ? <div>Loading...</div> : <div>Hiiii</div>}
              </Grid>

              <Grid container className={classes.lobbyContainer}>
                {lobbyLoading ? (
                  <div>Loading...</div>
                ) : (
                  <MiddleLobby contests={contests} />
                )}
              </Grid>
            </Box>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}

export default Lobby;
