import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {
    padding: theme.spacing(1)
  },
  multiple: {
    fontSize: 15,
    border: '2px solid',
    width: 30,
    height: 30,
    borderRadius: '50%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  img: {
    width: 30,
    minHeight: 30,
    maxHeight: 40
  },
  center: {
    textAlign: 'center'
  },
  roundedCorner: {
    borderRadius: '10px !importants'
  },
  table: {
    borderSpacing: '1px',
    borderCollapse: 'separate',
    fontWeight: 'bold',
    '& .MuiTableRow-root': {
      '& td:first-child': {
        borderRadius: '10px 0px 0px 10px'
      },
      '& td:last-child': {
        borderRadius: '0px 10px 10px 0px'
      },
      '& th:first-child': {
        borderRadius: '10px 0px 0px 10px'
      },
      '& th:last-child': {
        borderRadius: '0px 10px 10px 0px'
      }
    },
    '& .MuiTableCell-root': {
      borderBottom: '0px',
      fontWeight: 'bold'
    }
  }
}));
