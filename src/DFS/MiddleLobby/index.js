import React, { useEffect, useState } from 'react';
import {
  Grid,
  LinearProgress,
  Typography,
  TableContainer,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableSortLabel,
  Button
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import useStyles from './MiddleLobby.styles';

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    marginTop: 5,
    height: 5,
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor: theme.palette.background.default,
  },
  bar: {
    borderRadius: 5,
    backgroundColor: '#1a90ff',
  },
}))(LinearProgress);

const headCells = [
  { id: 'match_count', label: 'League', type: 'number' },
  { id: 'contest_name', label: 'Contest Name', type: 'string' },
  { id: 'start_date_time', label: 'Live', type: 'date' },
  { id: 'entries', label: 'Entries', type: 'number' },
  { id: 'prize_pool', label: 'Contest Prize', type: 'number' },
  { id: 'entry_fee', label: 'Entry Fee', type: 'number' },
];

function MiddleLobby(props) {
  const classes = useStyles();

  return (
    <TableContainer>
      <Table
        size="medium"
        className={classes.table}
        aria-label="simple table"
      >
        <TableHead>
          <TableRow className={classes.roundedCorner}>
            {headCells.map((headCell) => (
              <TableCell key={headCell.id}>
                <TableSortLabel>{headCell.label}</TableSortLabel>
              </TableCell>
            ))}

            <TableCell />
          </TableRow>
        </TableHead>
        <TableBody>
          {props.contests?.length ? (
            props.contests?.map((row) => (
              <TableRow key={row.contest_id} className={classes.roundedCorner}>
                <TableCell>
                  <Grid
                    container
                    direction="column"
                    alignItems="center"
                    justifyContent="center"
                    spacing={1}
                  >
                    <Grid item>
                      <img
                        alt={row.league_id}
                        src={`https://fantastic-gaming-s3.s3.ap-southeast-1.amazonaws.com/assets/leagues-icon/${row.league_abbr?.toLowerCase()}.png`}
                        className={classes.img}
                      />
                    </Grid>
                    <Grid item>
                      <Typography variant="caption">
                        {row.match_count} Matches
                      </Typography>
                    </Grid>
                  </Grid>
                </TableCell>
                <TableCell>
                  <Grid container direction="row" justify="space-between">
                    <Grid
                      item
                      xs={8}
                      style={{ cursor: 'pointer' }}
                    >
                      {row.contest_name}
                    </Grid>
                    <Grid item xs={4}>
                      {+row.max_join_limit > 1 && (
                        <Typography variant="h6" className={classes.multiple}>
                          M
                        </Typography>
                      )}
                    </Grid>
                  </Grid>
                </TableCell>
                <TableCell>
                  {/* {moment(row.start_date_time).format('MMM DD, YYYY h:mm A')} */}
                </TableCell>
                <TableCell>
                  {row.total_user_joined} of {row.contest_size}
                  <BorderLinearProgress
                    variant="determinate"
                    value={(row.total_user_joined / row.contest_size) * 100}
                  />
                </TableCell>
                <TableCell>
                  {parseFloat(row.prize_pool).toLocaleString('en-US', {
                    style: 'currency',
                    currency: row.supported_currency,
                  })}
                </TableCell>
                <TableCell>
                  {parseFloat(row.entry_fee).toLocaleString('en-US', {
                    style: 'currency',
                    currency: row.supported_currency,
                  })}
                </TableCell>
                <TableCell>
                  <Button
                    color="secondary"
                    variant="contained"
                    disableElevation
                  >
                    {/* {props.myContestParam?.status === '1' ? 'Edit' : 'Join'} */}
                  </Button>
                </TableCell>
              </TableRow>
            ))
          ) : (
            <TableRow>
              <TableCell colSpan={7} className={classes.center}>
                No contest found
              </TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default MiddleLobby;
