import axios from 'axios';

export const fetchLobbyMasterData = (url) => {
  return new Promise((resolve, reject) => {
    axios
      .get(
        'http://ec2-13-213-211-161.ap-southeast-1.compute.amazonaws.com/lobby/fetch_master_data'
      )
      .then((leaugeData) => {
        const {
          data: { Data },
        } = leaugeData;
        console.log('data====>', Data);
        resolve(Data);
      })
      .catch((error) => {
        console.log('error====>', error);
        reject(error);
      });
  });
};
