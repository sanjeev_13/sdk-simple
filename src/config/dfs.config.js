const API_URL =
  'http://ec2-13-213-211-161.ap-southeast-1.compute.amazonaws.com';

export const dfsConfig = {
  routes: {
    fetchMasterData: API_URL + '/lobby/fetch_master_data',
    fetchContestList: API_URL + '/lobby/fetch_contest_list',
  },
  contestListParams: {
    items_perpage: 100,
    current_page: 1,
    leagueId: 0,
    orderOn: 'contest_name',
    orderSequence: 'ASC',
    entryFee: { min: 0, max: '5000' },
    size: { min: '2', max: '10000' },
    prizePool: { min: 0, max: 28294 },
    search: '',
    gameType: '',
    prizeType: '',
    featured: '',
    currency: 'USD',
  },
};
