import axios from 'axios';

import { dfsConfig } from '../config';

export const fetchLobbyMasterData = (url) => {
  return new Promise((resolve, reject) => {
    axios
      .get(url, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then((leaugeData) => {
        const {
          data: { Data },
        } = leaugeData;
        console.log('data====>', Data);
        resolve(Data);
      })
      .catch((error) => {
        console.log('error====>', error);
        reject(error);
      });
  });
};

export const fetchLobbyContestList = (params) => {
  return new Promise((resolve, reject) => {
    axios
      .post(dfsConfig.routes.fetchContestList, params, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: undefined,
        },
      })
      .then((contestList) => {
        const {
          data: { Data },
        } = contestList;
        resolve(Data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};
