import React from 'react';
import ReactDOM from 'react-dom';

import Lobby from './DFS/Lobby';
import { fetchLobbyMasterData } from './api';
import axios from 'axios';

class App {
  init(config) {
    const { url, layoutId } = config;
    axios
      .get(
        'http://ec2-13-213-211-161.ap-southeast-1.compute.amazonaws.com/lobby/fetch_master_data'
      )
      .then((leaugeData) => {
        const {
          data: { Data },
        } = leaugeData;
        console.log('data====>', Data);
        // resolve(Data);
      })
      .catch((error) => {
        console.log('error====>', error);
        // reject(error);
      });

    return this.getLayout(layoutId);
  }

  getLayout(layoutId) {
    return ReactDOM.render(<Lobby />, layoutId);
  }
}

export default new App();
